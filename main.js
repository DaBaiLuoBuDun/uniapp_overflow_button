import Vue from 'vue'
import App from './App'
import OverflowButton from "./components/overflow-button/overflow-button.js"
Vue.config.productionTip = false

App.mpType = 'app'

Vue.prototype.OverflowButton=OverflowButton;

const app = new Vue({
    ...App
})
app.$mount()
